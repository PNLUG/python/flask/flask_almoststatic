from setuptools import setup

setup(
    name='Flask-Almoststatic',
    version='0.1',
    url='https://www.pnlug.it',
    license='MIT',
    author='Claudio Driussi',
    author_email='claudio.driussi@gmail.com',
    description='Flask extension used to build sites from static contents.',
    long_description=open('README.rst').read(),
    packages=['flask_almoststatic'],
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[
        'Flask',
        'PyYaml',
        'Markdown'
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Framework :: Flask',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Programming Language :: Python :: 3',
        'Development Status :: 4 - Beta'
    ],
)
