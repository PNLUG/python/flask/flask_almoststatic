import os
import time
import yaml
import markdown
import flask


class AlmostStatic():
    def __init__(self, app=None):
        self.config = {}
        self.cached = {}
        self.extra_args = {}
        self.pages = {}
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        """Add some config parameters and rules to the app to bind the
        extension"""
        self.app = app
        app.config.setdefault('FAS_CONTENT', 'content')
        app.config.setdefault('FAS_CONFIG', 'config.yaml')
        app.config.setdefault('FAS_PAGES', 'pages')
        app.config.setdefault('FAS_PAGES_URL', '/')
        app.config.setdefault('FAS_PAGES_TEMPLATE', 'page.html')
        app.config.setdefault('FAS_BLOG', 'blog')
        app.config.setdefault('FAS_BLOG_URL', '/blog/')
        app.config.setdefault('FAS_BLOG_TEMPLATE', 'blog.html')
        app.config.setdefault('FAS_MEDIA', 'media')
        app.config.setdefault('FAS_ENVELOPE', None)
        app.config.setdefault('FAS_CACHEABLE', True)
        app.config.setdefault('FAS_DATE_FORMAT', '%m-%d-%Y')
        # add a rule for serving static files from content data path. The
        # config parameter "FAS_CONTENT" must be set before calling this method
        # if not set it keeps the default value
        rule = '/%s/<path:filename>' % app.config['FAS_CONTENT']
        app.add_url_rule(rule, 'content', self.content_static)
        # add a custom content processor to add some filters and global data
        self.content_processor()
        app.template_context_processors[None].append(self.content_processor)

    def _content_root(self):
        """get the content root of fas system, the path can be absolute or
        relative from current dir."""
        return os.path.join(self.app.root_path, self.app.config['FAS_CONTENT'])

    def content_static(self, filename):
        """The content directory is rendered as static, files can have a
        relative path from there
        """
        return flask.send_from_directory(
            os.path.join(self._content_root()),
            filename)

    def content_processor(self):
        """Add some global contents and functions availables for all templates
        """
        # if already set skip loading values
        if self.config:
            return self.config
        # the file "config.yaml" must be set - usually it contains menu's and
        # global config parameters
        fname = os.path.join(self._content_root(),
                             self.app.config['FAS_CONFIG'])
        with open(fname) as f:
            self.config = yaml.load(f)
            f.close()
        # add some function to jinja2 templates
        self.config['enum_list'] = enum_list
        self.config['get_media'] = self.get_media
        self.config['get_markdown'] = get_markdown
        return self.config

    def get_media(self, media_obj, widget={}):
        """Get the filename of static media file of a widget. Start point of
        media files usually is: ./content/media, but in the widget we can add
        a additional relative media path."""
        filename = os.path.join('/', self.app.config['FAS_CONTENT'],
                                self.app.config['FAS_MEDIA'],
                                widget.get('media_path', ''), media_obj)
        return filename

    def get_filename(self, pagename, dir_pages=None):
        """get full filename form the name of page, the file has the same name
        of page with .yaml extension and is searched in the content dir"""
        if not dir_pages:
            dir_pages = self.app.config['FAS_PAGES']
        return os.path.join(self._content_root(), dir_pages, pagename+'.yaml')

    def load_pages(self):
        """Load all pages from disk. and store them in self.pages indexed by
        url, in the pages are added the following properties:
        template: is the template used to render the page.
        is_blog: distinguish betwen blog and ordinary pages
        The loading is required each time the pages changes"""
        self.pages = {}
        self._loadpages(self.app.config['FAS_BLOG'],
                        self.app.config['FAS_BLOG_URL'],
                        self.app.config['FAS_BLOG_TEMPLATE'])
        self._loadpages(self.app.config['FAS_PAGES'],
                        self.app.config['FAS_PAGES_URL'],
                        self.app.config['FAS_PAGES_TEMPLATE'])

    def _loadpages(self, pages_dir, pages_url, template, root_dir=None):
        """support method for load_pages"""
        dir_pages = os.path.join(self._content_root(), pages_dir)
        if not root_dir:
            root_dir = dir_pages + os.path.pathsep
        for fname in os.listdir(dir_pages):
            fullname = os.path.join(dir_pages, fname)
            if os.path.isfile(fullname):
                with open(fullname) as f:
                    page = yaml.load(f, Loader=yaml.FullLoader)
                    f.close()
                if not page.get('active', True):
                    continue
                url = fullname[len(root_dir):]
                url = pages_url + url.split('.')[0]
                page['template'] = template
                page['is_blog'] = \
                    template == self.app.config['FAS_BLOG_TEMPLATE']
                self.pages[url] = page
            else:
                self._loadpages(fullname, pages_url, template, root_dir)

    def build_page(self, pagename, **kwargs):
        """build a page from page declarations"""

        self.extra_args = kwargs
        if not self.pages:
            self.load_pages()

        # the pagename is passed from flask without the leading slash
        page = self.pages.get('/'+pagename)
        if not page:
            return None

        # check if content should be enveloped into non static content.
        envelope = page.get('envelope')
        if envelope is None:
            envelope = self.app.config['FAS_ENVELOPE']

        # if the page was already cached and newer the file it is not rendered
        # again
        content = None
        if page.get('cacheable', self.app.config['FAS_CACHEABLE']):
            content = self.cached.get(pagename)

        # the main render logic
        if not content:
            content = self.render_content(page['content'])
            if not envelope:
                content = flask.render_template(
                    page['template'],
                    content=content, page=page, **self.extra_args)
            self.cached[pagename] = content

        # envelope the content into a non static content
        if envelope:
            # content = flask.render_template(envelope, content=content)
            content = flask.render_template(
                page['template'], content=content, page=page,
                **self.extra_args)

        return content

    def render_content(self, content):
        """Render the content of page, the content is a a key of the page and
        can be a single widget or a list of widgets
        """
        def render_widget(widget, self):
            # each widgets has a key 'type' which indicate the name of template
            # to render, of if the type id 'include', a file of widgets is
            # included
            if widget['type'] == 'include':
                return self._import_file(widget['file'])
            else:
                widget = self.eval_widget(widget)
                return flask.render_template(widget['type']+'.html',
                                             widget=widget, **self.extra_args)
        if isinstance(content, list):
            s = ""
            for widget in content:
                s += render_widget(widget, self)
        else:
            s = render_widget(content, self)
        return s

    def eval_widget(self, value, key=''):
        """Eval a single widget searching for a 'text' key which contain the
        text render. If the text is a string it is always rendered as markdown,
        if text is a dict, it can contain the key 'include' whic allow to
        include a file and/or 'content' which is rendered as a page content
        """
        if isinstance(value, list):
            for i, v in enumerate(value):
                value[i] = self.eval_widget(v)
        if isinstance(value, dict):
            for k, v in value.items():
                value[k] = self.eval_widget(v, k)
        if key == 'text':
            if isinstance(value, dict):
                text = ""
                if value.get('include'):
                    text += self._import_file(value['include'])
                if value.get('content'):
                    text += self.render_content(value['content'])
                value = text
            else:
                value = markdown.markdown(value, extensions=['tables'])
        return value

    def _import_file(self, filename):
        """Allow to include files containg widgets or plain html or markdown
        files. Nested inclusions are allowed and the limit is only the python
        call stack, so you have only to avoid circular inculsions.
        the recognized extensions are: '.html' for plain html text, '.md' for
        markdown text and '.yaml' which can contain widgets under 'content' key
        """
        fname = os.path.join(self._content_root(), filename)
        if not os.path.isfile(fname):
            return 'The file: %s does not exists.' % filename
        with open(fname) as f:
            if fname.endswith('.html'):
                s = f.read()
            elif fname.endswith('.md'):
                s = f.read()
                s = markdown.markdown(s, extensions=['tables'])
            elif fname.endswith('.yaml'):
                content = yaml.load(f, Loader=yaml.FullLoader)
                s = self.render_content(content)
            f.close()
            return s
        return "File: %s not recognized type." % filename


def enum_list(l):
    """add enumerate function to jinja2"""
    return enumerate(l)


def get_markdown(text):
    return markdown.markdown(text, extensions=['tables'])
