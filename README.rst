===================
 Flask-Almoststatic
===================

THIS SOFTWARE IS UNDER DEVELOPMENT, SO LINKS ARE NOT ALL IMPLEMENTED YET

About
=====
Flask-Almoststatic is a Flask extension that can be used to build sites or
portions of sites from static contents written in yaml and Markdown languages

Installation
============
Flask-Almoststatic is on PyPI so all you need is: ::

    pip install Flask-Almoststatic

Documentation
=============
Documentation is readable at http://flask-almoststatic.readthedocs.org or can be built using Sphinx: ::
