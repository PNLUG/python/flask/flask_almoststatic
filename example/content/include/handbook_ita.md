# MANUALE PER L'UTILIZZO DI FLASK-ALMOSTSTATIC

*THis is the italian version of the handbook, it's not completed yet and it will be translated later.*

## Quick Start

Se avete fretta di vedere che cosa fa il componente per valutare se vale la pena di approfondire, vi consgliamo di scaricare ed avviare il progetto git utilizzando i seguenti comandi.

Se risulta di vostro gradimento potete proseguire.

## Siti dinamici e statici

La maggior parte dei siti in internet è costuita con strumenti dinamici, molti su piattaforma LAMP. Spesso questo è un overkill perché molti siti in realtà sono composti da pagine il cui contenuto non cambia spesso, ma essendo basati su tecnologie dinamiche vengono comunque costruiti ogni volta che viene fatta una richiesta. Questo comporta un inutile spreco di risorse in termini di CPU e di RAM ed inoltre la presenza di molti componemti tecnici abbassa la robustezza e la scalabilità.

Per questo motivo ultimamente si sta diffondendo l'uso dei *"static site generators"* tra i quali citiamo [jekyll](https://jekyllrb.com/) e [hugo](https://gohugo.io/) i quali utilizzando alcune regole e files di markup permettono di generare siti in puro html che hanno l'evidente vantaggio di essere molto leggeri e scalabili e di avere pochi requisiti tecnologici, in pratica necessitano solo del server http. Ovviamente in questo caso viene sacrificata la versatilità.

Oltre alle pagine statiche i generatori dispongono di meccanismi per la costruzione di blog e quindi permettono di gestire ordini cronologici, categorie e tags, in questo modo avvicinano il loro look a quello dei siti dinamici.

Flask-almoststatic è un componete per il microframework [Flask](https://flask.palletsprojects.com) che si pone a metà tra i due approcci, anzi è più orientato verso le pagine statiche, ma lascia comunque a disposizione tutte le risorse di flask per gestire contenuti dinamici in caso di necesità.

Il vantaggio è che comunque le pagine statiche una volta costuite vengono rese disponibili in modo velocissimo e con uso minimo delle risorse. Non è richiesto alcun database ed anche questo aumenta la robustezza e le prestazioni.

## Installazione

L'installazione si fa esattamente come tutti gli altri componenti per flask. Oltre a flask sono richiesti i moduli PyYAML e Markdown. Trattandosi di un componente che utilzza configurazioni articolate, viene messo a disposizione un esempio di applicazione pronta all'uso che ne dimostra le funzionalità

## Applicazione Flask

Una installazione minimale oltre alle classiche cartelle `templates` e `static` previste da flask contiene la cartella `content` in cui verranno memorizzati gli script per configurare le pagine statiche e gli articoli del blog, si possono anhe inserire contenuti multimendiali abbinati alle pagine.

È anche richiesta la presenza di un file di nome `config.yaml` che contiene alcuni dati di configurazione come ad esempio i menù, il nome del sito ecc...

La cartella è anche il punto di partenza per altri contenuti come ad esempio files di inclusione.

    .
    ├── flaskapp.py
    ├── content
    │   ├── config.yaml
    │   ├── blog
    │   ├── include
    │   ├── media
    │   └── pages
    ├── static
    └── templates

I contenuti vengono configurati da script in formato yaml che hanno una propria sintassi che esporremo tra breve. Invece per quanto riguarda l'applicazione flask illustriamo ora un progetto minimale.

    from flask import Flask
    from flask import render_template
    from flask import make_response
    from flask_almoststatic import AlmostStatic

    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'my_super_secret'

    fas = AlmostStatic(app)

    @app.route('/<page>')
    def fas_pages(page):
        page_text = fas.build_page(page)
        if not page_text:
            return page_not_found(404)
        return make_response(page_text)

    @app.route('/')
    def index():
        return fas_pages('home')

    @app.errorhandler(404)
    def page_not_found(error):
        return render_template('page404.html', page={}), 404

    # classic flask routing
    @app.route('/myroute')
    def myroute():
        return make_response(render_template('my_page.html', page={}))

Il blog richiede alcune dichiarazioni di routing di cui ci occuperemo più avanti.

Le pagine statiche vegono preparate con il comando:


    @app.route('/<page>')
    def fas_pages(page):
        page_text = fas.build_page(page)
        return make_response(page_text)

che dato il nome di un routing cerca tra le pagine dei contenuti un file yaml con lo stesso nome e costruisce la pagina html. Un esempo semplice di file yaml potrebbe essere:

    ---
    title: Test page
    date: 2999-12-31
    author: Claudio Driussi
    tags: [pages, test, home]
    extends: base.html
    cacheable: True

    content:
    - type: text_markdown
      id: md-01
      text: |
        ## h2 header md-01
        my text

    - type: text_markdown
      id: md-02
      text: |
        ## h2 header md-02
        my text

Vediamo ora come sono strutturate le pagine.

## Le pagine

Le pagine ordinarie e gli articoli del blog sono molto simili e differiscono soprattutto per come sono organizzate.

La pagina si compone di una header con alcuni campi e di un contenuto di nome `content`. Alcuni campi vengono utilizzati dal sistema, altri invece servono solo per dare indicazioni ai templates Jinja.

il campo `content` di solito è una lista di "widgets" che devono avere un corrispondente file di template per essere renderizzati.

Nella header i campi previsti sono:

- `title` Può essere usato per dare il titolo html e per le liste di articoli del blog
- `date` Si usa per liste cronologiche del blog
- `author` Raggurppamento blog
- `categories` Lista delle categorie per ragruppamento blog
- `tags` Lista dei tags per ragruppamento blog
- `extends` Solitamente le pagine e gli articoli del blog ereditano da una pagina di default, ma in questa posizione si può dichiarare una pagina diversa.
- `cacheable` Indica se la pagina deve essere ricostruita ogni volta o se può essere mantenuta in memora. Se si usa la cache, ogni volta che si modificano le pagine si deve riavviare il server.
- `envelope` I contenuti nella cache possono essere comunque racchiusi all'interno di un contenuto dinamico dichiarato con questo campo.
- `image` Immagine da presentare nei titoli del blog
- `intro` Testo introduttivo da inserire nei titoli del blog
- `content` La lista dei widget che compongono il contenuto della pagina.

Tra questi campi il solo strettamente indispensabile è `content` gli altri spesso sono consigliati ma non limitano il funzionamento del programma.

Oltre ai campi previsti si possono aggiungere altri campi che possono essere usati dai templates. Infatti ad ogni template viene passata l'intera pagina sotto forma di dizionario ad esempio questo è un tipico esempio di template di pagina.

    {%extends page.extends or "base.html" %}
    {%block title%}{{page.title}}{% endblock %}
    {%block author%}{{page.author}}{% endblock %}
    {%block body%}
    <main role="main">
      {{ content|safe }}
    </main>
    {%endblock%}

Da notare il filter `{{ content|safe }}` che istruisce Jinja ad accettare i tags html nel modo in cui vengono passati, questo serve perché i contenuti vengono costruiti esternamente ai templates, ma significa anche che purtroppo ci sono alcune limitazioni alla stintassi degli script markdown che non accettano i costrutti più complessi come le tabelle e i testi di codice.

Per fortuna il sistema per la costruzione dei widgets è molto flessibile ed in ogni caso si può inserire nel testo direttamente il codice html.

## I widgets

Come dicevamo in precedenza i contenuti della pagina sono all interno di un campo `content` che è una lista di `widgets`. In pratica una pagina è composta da "fette" ed ognuma di questa è un widget.

Un widget è composto da campi, ad esempio:

    content:
    - type: text_markdown
      id: simple_md2
      text: |
        ## h2 header
        embedded **markdown** component

    - type: text_markdown
      id: simple_md2
      text: |
        <h2>h2 header</h2>
        <p>embedded html text</p>

Nei widgets gli unici campi che hanno una funzione specifica sono:

**`type`** indica i tipo di widget, è necessario che esista un template con lo stesso nome e con estensione .html ad esempio se il widget è di tipo `text_markdown` dovrà esistere un template di nome `text_markdown.html` a cui viene passato il dict con tutti i campi del widget che possono essere usati liberamente.

**`text`** non è indispensabile, ma se presente contiene un testo markdown da passare alla pagina. la particolarità è che se invece di un testo si passa un dizionario, si può includere altri files oppure contenuti nidificati, ad esempio:

    content:
    - type: text_markdown
      id: embedding
      text:
        include: include/include1.yaml
        content:
        - type: text_html
            id: simple_html
            text: |
            <h1>Content embedded</h1>
        - type: tabbed
            tabs:
            - label: nested1
                active: True
                text: |
                **nested tab 1**
            - label: nested2
                text: |
                **nested tab 2**

I campi riconosciuti da `text` sono `include` che permette di includere un file esterno  e `content` che permette di includere direttamente un contenuto yaml.

Un tipo particolare di widget è il tipo `include` che permette di includere contenuti provenienti da altri files, questo ad esempio consente di ripetere gli stessi contenuti su più pagine.

    - type: include
      file: include/include.yaml

    - type: include
      file: include/include.html

    - type: include
      file: include/include.md

Il funzionameto dei files di inclusione dipende dalla estensione del file. Vengono riconosciuti tre tipi di estensione. I files `.yaml` contengono widgets esterni, i files `.md` e `.html` contengono testi semplici nei rispettivi formati.

Il sistema di inclusione sia per il campo `text` che per il widget `include` è molto potente in quanto permette inclusioni nidificate di qualsiasi livello, si deve solo fare attenzione ad evitare i riferimenti "circolari", ad esempio se il file `include.yaml` include a sua volta il file `include.yaml` si entra in un circolo vizioso che termina solo con l'esaurimento delle risorse di ricorsione del linguaggio.

Tutti gli altri campi sono liberi e si possono progettare in funzione del rendering dei templates. Comunque ci sentiamo di consigliare alcune convenzioni. Ad esempio:

- `id` può essere usato per assegnare un id al widget per configurare i css
- `style` si possono inserire delle direttive css inline
- `class` da usare come selettore di classe
- `media_path` si può usare per inserire il prefisso di ricerca dei contenuti multimediali

ad esempio un semplice template per il widget "text_markdown.html" potrebbe essere:

    {% if widget.style -%}
    <style>
        #{{widget.id}} {
        {{widget.style|safe}}
        }
    </style>
    {% endif -%}
    <span id="{{widget.id}}">
    {{widget.text|safe}}
    </span>

Per il resto, grazie alla potenza di Jinja l'unico limite è la vostra fantasia, ma in ogni caso potete partire dagli esempi presenti nel progetto demo.

## La struttura del sito

## La configurazione del componente

## Il file config.yaml

## Le pagine

## Il blog

## I contenuti non statici


