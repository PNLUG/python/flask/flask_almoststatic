import time
import datetime

from flask import Flask
from flask import render_template
from flask import make_response

from flask_almoststatic import AlmostStatic

app = Flask(__name__)

app.config['SECRET_KEY'] = 'super_sectet_key'

app.config['FAS_CONTENT'] = 'content'
app.config['FAS_CACHEABLE'] = False  # in production comment this line
app.config['FAS_DATE_FORMAT'] = '%d/%m/%Y'  # european format

fas = AlmostStatic()
fas.init_app(app)


@app.route('/<path:page>')
def fas_pages(page):
    page_text = fas.build_page(page)
    if not page_text:
        return page_not_found(404)
    return make_response(page_text)


@app.route('/update_pages')
def update():
    """Each time the content of pages and blog aticles are changed a reload
    is required."""
    fas.load_pages()
    return fas_pages('home')


@app.route('/')
def index():
    return fas_pages('home')


@app.route('/dynamic')
def dynamic():
    ts = time.time()
    dd = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d')
    tt = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S')
    page_text = fas.build_page('dynamic',
                               dynamic='This is a dynamic content!',
                               t_date=dd,
                               t_time=tt)
    if not page_text:
        return page_not_found(404)
    return make_response(page_text)


@app.errorhandler(404)
def page_not_found(error):
    return render_template('page404.html', page={}), 404


@app.route('/flask_route')
def flask_route():
    """You can mix Almoststatic pages and standard routend pages, the standard
    route hide the Almoststatic routing system"""
    resp = make_response(render_template('flask_route.html', page={}))
    return resp


@app.context_processor
def content_processor():
    """A simple content processor, you can customize your brand"""
    d = {}
    d['brand'] = custom_brand
    return d


def custom_brand(caption):
    """Simple brand customization"""
    return caption
